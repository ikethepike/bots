<?php

require_once 'user.php';

foreach(range(2, 101) as $i ) {
	$user = new User(false, $i);

	$interests 	= $user->data();
	$bio 		= $user->bio();

	echo "<hr>";
	echo $bio['first_name'] . "<br>";
	echo $bio['surname'] . "<br>";
	echo $bio['simple email'] . "@wearepanopticon.com" .  "<br>";
	echo $bio['dob'] . "<br>";

	foreach($interests as $interest) {
		$url_term = urlencode($interest);
		echo "<b> <a href='https://facebook.com/search/pages/?q={$url_term}' target='_blank'>" . $interest . "</a></b><br>";
	}


}