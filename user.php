<?php

require_once 'vendor/autoload.php';
require_once 'session.php';



class user {

	private $_db = null,
			$_interests,
			$_possibilities = [],
			$_bio;

	public function __construct($new = false, $user = null){
		$host 		= '127.0.0.1';
		$dbName 	= "bots";
		$username 	= "root";
		$password 	= '';

		if(!is_bool($new)) {
			die('$new must be boolean');
		}

		// Create new PDO
		try{

			$this->_db = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $username , $password );

		} catch(PDOException $e) {
			echo 'Fatal Database Error';
			die( $e->getMessage() );
		}

		// Start Session if possible
		if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		}

		if(isset($user)) {
			Self::current($user);
			Session::set('user', $user);



		} elseif(!Session::exists('user') || $new ){
			// Creates new bot

			Self::interests();
			Self::create($this->_possibilities);
			Session::set('user', $this->_bio['index']);

		} else {
			Self::current(Session::get('user'));
		}

	}



	private function interests(){
		include 'interests.php';
		$this->_possibilities = $interests;
	}

	public function create( array $interests) {

		$faker = Faker\Factory::create();

		// Construct User bio
		$first_name = $faker->firstName();
		$surname 	= $faker->lastName();
		$age 		= rand(21, 65);
		$month 		= $faker->monthName;
		$day		= rand(1, 28);
		$year 		= 2015 - $age;
		$telephone  = "+447" . Self::number(9);

		// Assign unique ID

		$rows = $this->_db->query('select count(*) from bots')->fetchColumn();
		$bot_id = $rows + 1;


		$name = $first_name . " " . $surname;

		$email = $bot_id . "_" . $first_name.$surname. "." . $age . "@gmail.com";

		$simple_email = $bot_id . $first_name;

		$address = $faker->address();

		$dob 	= $day . "/" . $month . "/" . $year;

		$bio = ['first_name' => $first_name, 'surname' => $surname, 'fullname' => $name, 'email' => $email, 'simple email' => $simple_email, 'age' => $age, "dob" => $dob, 'address' => $address, 'telephone' => $telephone, 'index' => $bot_id ];

		$this->_bio = $bio;


		$user_interests = Self::get_random($interests , 10);

		// Create user ID card

		$id_card = fopen('users/user_'.$bot_id.".html", 'w');


		$content = "<ul><li><h2> Name </h2><br>". $name . "</li><li><h2> Age </h2><br>" . $age ."</li><li><h2> Email </h2><br>" . $email ."</li><li><h2> Simple Email </h2><br>". $simple_email . " </li><li><h2> Address </h2><br>".$address. "</li><li><h2> Telephone </h2><br>" . $telephone ."</li></ul> <h2> Interests </h2><br>" . implode(", ", $user_interests) ;

		fwrite($id_card, $content);

		$this->_interests = $user_interests;



		// Insert into DB

		$bio = json_encode($bio);
		$user_interests  = json_encode($user_interests);

		$sql = "INSERT INTO bots(bio,interests, bot_key) VALUES (:bio, :interests, :bot_key)";

		$insert = $this->_db->prepare($sql);

		$insert->bindParam(':bio', $bio, PDO::PARAM_STR);
		$insert->bindParam(':interests', $user_interests, PDO::PARAM_STR);
		$insert->bindParam(':bot_key', $bot_id, PDO::PARAM_STR);

		if($insert->execute()){
			return true;
		} else {
			die("Error inserting data!");
			return false;
		}

	}


	public function data(){
		return (array) $this->_interests;
	}

	public function bio(){
		return (array) $this->_bio;
	}

	public function add_interests($amount = 10) {

		if(empty($this->_possibilities)){
			self::interests();
		}

		if(!is_numeric($amount)) {
			return false;
		}

		$new_interests = Self::get_random($this->_possibilities , $amount, $this->_interests);

		$this->_interests = array_merge($this->_interests, $new_interests);



		$user_interests  = json_encode($this->_interests);

		$sql = "UPDATE `bots` SET `interests` = :interests WHERE `bot_key` = :bot_key";

		$update = $this->_db->prepare($sql);
		$update->bindParam(':interests', $user_interests);
		$update->bindParam(':bot_key', Session::get('user'));



		if($update->execute()){
			echo "data inserted successfully! <hr>";
		} else {
			die("Error inserting data!");
			return false;
		}

		return $this->_interests;
	}

	private static function get_random(array $interests, $amount = 10, $array_check = [], $string = false){

		$array = [];
		$count = count($interests);

		if($count){
			foreach ($interests as $interest) {

				if(count($interest)){
					foreach ($interest as $subinterest) {
						$array[] = $subinterest;
					}
				}
			}
		}

		$total = count($array) - 1;

		$i = 0;
		$output = [];
		while ( $i <= $amount) {
			$number = rand(0, $total);
			if( !in_array($array[$number], $output) && !in_array($array[$number], $array_check) ) {
				$output[] = $array[$number];
				$i++;
			}
		}

		if($string){
			return implode(', ', $output);
		} else {
			return $output;
		}
	}


	public function number( $length) {
		    $result = null;

		    for($i = 0; $i < $length; $i++) {
		        $result .= mt_rand(0, 9);
		    }
		    return $result;
	}


	public function current($id = null) {

		if(isset($id)) {

			if(Session::get('user')){
				$query = $this->_db->prepare("SELECT * FROM `bots` WHERE `bot_key`= ? LIMIT 1");
				$query->bindParam(1, $id);
				$query->execute();
				$user = $query->fetch();

				if(empty($user)){
					return false;
				} else {
					$this->_bio 		= json_decode($user['bio']);
					$this->_interests 	= json_decode($user['interests']);

				}


			} else {
				return false;
			}

		}

	}


} // User end


class Result {

	private $_collection,
			$_comparison;

	public function __construct(array $collection) {
		if(isset($collection)) {
			$this->_collection = $collection;
		}
	}

	public function map($map = null, $comparison = null) {

		if($map && is_array($map)) {
			$this->_collection = $map;
		} elseif($map && !is_array($map)) {
			return false;
		}

		$depth = self::depth($this->_collection);

		return self::table($depth, $this->_collection, $comparison);


	}

	public function lists(array $array, $title = null, $number = null) {
		if(!isset($array)) {
			return false;
		}

		$current = $this->_collection;
		
		sort($array);
		sort($current);

		echo "<div style='width:750px;position:relative;margin:auto;'>";

		echo (!empty($title)) ? "<h1>{$number} {$title}</h1><br>" : null ;

		echo "<ul style='float:left;width:50%;margin:0;padding:0;'><h2> Input </h2>";
		foreach($current as $entry) {
			echo "<li style='margin:4px 0;'>{$entry}</li>";
		}
		echo "</ul>";


		echo "<ul style='float:left;width:50%;margin:0;padding:0;'> <h2> Output </h2> ";
		foreach($array as $entry) {
			echo "<li style='margin:4px 0;'>{$entry}</li>";
		}
		echo "</ul></div>";

	}

	private function table($depth = 1, array $map, $comparison) {

		echo "<table style='width:100%;'>";
		echo "<style> table, td, tr, th { border:1px solid gray;position:relative;overflow:hidden;} td, th { min-width:6em; height:2.5em; } .correct {background:green;font-weight:bold} .incorrect, .correct {width:100%;height:100%;color:white;display:block;padding:5px;text-align:center;} .incorrect {background:red;line-height:2.5em;} </style>";

		if(isset($comparison)) {


			if($depth > 1) {

				$map_values			= self::flatten($map);
				$comparison_values 	= self::flatten($comparison);
				$total 				= count($map_values) + count($comparison_values);

				print_r($map_values);

				// X-axis headers

				echo "<th>Total: {$total}</th>";
				foreach ($map as $parent_index => $value) {
					echo "<th> {$parent_index} </th>";
					foreach ($value as $index => $value) {
						echo $index;
					}
				}

				// Y-axis headers

				foreach ($comparison as $key => $value) {
					echo "<tr><th>". $key . "</th></tr>";
				}



			} else {

				$difference = array_diff($map, $comparison);
				$intersect  = array_intersect($map, $comparison);
				$entries 	= count($map) + count($comparison);
				$percentage = ( count($intersect) / $entries * 100 );
				$total 		= count($map) + count($comparison);

				$total_matches 		= [];
				$total_differences	= [];

				foreach($map as $value) {
					foreach($comparison as $check) {
						$check = $check;
					}
					if(Self::likeness($value, $check) > 90) {
						$total_matches[] = $value;
					} else {
						$total_differences[] = $value;
					}	
				}

				print_r($total_differences);

				if(!empty($intersect)) {
					echo "<h3> Matches </h3><br><table><tr>";
					foreach($total_matches as $index => $value):
						echo "<td> {$value} </td>";
					endforeach;
					echo "</tr></table>";
				} else {
					echo "<h3> No Intersections </h3>";
				}

				if(!empty($intersect)) {
					if(!empty($difference)) {
						echo "<h3> Differences </h3><br><table><tr>";
						foreach($difference as $index => $value):
							echo "<td> {$value} </td>";
						endforeach;
						echo "</tr></table>";
					} else {
						echo "<h3> No differences </h3>";
					}
				}

				echo "<hr><h2> Confusion Matrix </h2>";
				echo "<table><th>Total: {$total}</th>";
				foreach($map as $index) {
					$values[] = $index;
					echo "<th>{$index}</th>";
				}

				$matches = 0;

				foreach ($comparison as $key) {
					echo "<tr><th>". $key . "</th>";
					foreach($values as $index) {
						echo "<td>" . ((Self::likeness($key, $index) > 90) ? "<span class='correct'> ✓ </span>" : "<span class='incorrect'> X </span>") . "</td>";
						if(Self::likeness($key, $index) > 90) { $matches++; }
					}
					echo "<tr>";
				}

				echo "</table>";

				$alignment = ($matches / $total) * 100; 
				(empty($intersect)) ? $intersect = 0 : $intersect = $intersect;
				(empty($difference)) ? $difference = 0 : $difference = $difference;
 
				$results = ['Number of matches' => $matches, 'matches' => $intersect, 'differences' => $difference, 'Alignment' => $alignment . "%", 'Percentage match' => $percentage . "%", 'Combined entries' => $entries];
				return $results;
			}


		} else {

			if($depth == 1) {

				foreach($map as $index => $value):
					echo "<td> {$value} </td>";
				endforeach;
			} else {
				$total = 0;
				foreach($map as $index => $value):
					echo "<tr><th style='font-weight:bold;'> {$index} </th>";
						$total = count($value);
						foreach($value as $index => $value){
							echo "<td>{$value}</td>";
						}
					echo "<td style='background:red;font-weight:bold'> Line total: {$total} </td></tr>";
				endforeach;
			}

		}

		echo "</table>";
	}

	private function likeness($input, $check ) {
		
		$input = explode(' ', strtolower($input) );
		$check = explode(' ', strtolower($check) );

		if(is_array($check) && is_array($input)) {
			$i = 0;
			foreach ($input as $key) {
				if(in_array($key, $check)) {
					$i++;
				}
			}

			$count = count($check);
			$match = ($i / $count) * 100; 

			return $match;
		}

	}

	private function depth(array $array) {

		    $max_indentation = 1;

		    $array_str = print_r($array, true);
		    $lines = explode("\n", $array_str);

		    foreach ($lines as $line) {
		    	$indentation = (strlen($line) - strlen(ltrim($line))) / 4;

		    	if ($indentation > $max_indentation) {
		    		$max_indentation = $indentation;
		    	}
		    }

		    return ceil(($max_indentation - 1) / 2) + 1;
	}

	private function flatten($array){
		   $flat = array();

		   foreach ($array as $value) {
		           if (is_array($value)) $flat = array_merge($flat, self::flatten($value));
		           else $flat[] = $value;
		   }
		   return $flat;
	}

	public function dumb($array, $array2) {
		foreach($array as $entry) {
			if(in_array($entry, $array2)) {
				$matches[] = $entry;
			}
		} 
		return $matches;
	}
}