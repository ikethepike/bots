<?php

    require_once 'vendor/autoload.php';
    require_once 'user.php';
    require_once 'interests.php';

    $GLOBALS['uagent'] = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3";
    $GLOBALS['debug'] = true;


    function fb_login($email, $password) {

        if($GLOBALS['debug']) {
            echo "<hr> <h3> Begin Login sequence with login: {$email} and password: {$password} </h3><br><hr>";
        }

        $cookie = "cookies/{$email}.txt";
        $cookie_file = fopen($cookie, "w");
        $GLOBALS['cookies'] = $cookie;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://m.facebook.com/login.php');
        curl_setopt($ch, CURLOPT_POSTFIELDS,'charset_test=€,´,€,´,水,Д,Є&email='.urlencode($email).'&pass='.urlencode($password).'&login=Login');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept-Charset: utf-8','Accept-Language: en-us,en;q=0.7,bn-bd;q=0.3','Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5'));
        curl_setopt($ch, CURLOPT_COOKIEJAR, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $GLOBALS['uagent']);
        curl_setopt($ch, CURLOPT_REFERER, "https://.m.facebook.com");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 600);

        $result = curl_exec($ch) or die(curl_error($ch));

        curl_close($ch);

        return $result;

    }

function fb_search($term, $id = false) {

    if($GLOBALS['debug']) {
        echo "<br> <b> Started FB Search Method </b> <hr>";
    }

    homepage();

    $url_term   = urlencode($term);
    $compact    = str_replace(" ", "", strtolower($term));
    $compact    = preg_replace('/[^a-zA-Z0-9_%\[().\]\\/-]/s', '', $compact);
    $search_url = 'https://facebook.com/search/pages/?q='.$url_term;

    $seps   = " _+-:*^";
    $string_start       = strtok(strtolower($term), $seps);

    echo "<H1> {$GLOBALS['cookies']} </h1>";

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30000);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $GLOBALS['cookies']);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $GLOBALS['cookies']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $GLOBALS['uagent']);
    curl_setopt($ch, CURLOPT_REFERER, 'http://facebook.com');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $search_url);
    curl_setopt($ch, CURLOPT_HTTPGET, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'GET');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6000);

    $response = curl_exec($ch);
    curl_close($ch);

    $html = $response;

    $value = preg_match_all('/<div id="initial_browse_result">(.*?)<\/div>/is',$response, $search );


    if($GLOBALS['debug']){
        echo "<h1> New Search: " .$term . "</h1><br>";
        echo 'Term is: '.$term . "<br> The url was: <a href='{$search_url}' target='_blank'>" . $search_url . "</a><br><hr>";
    }

    // Find all Links

    $output = [];
    $final  = [];

    if( $response ){

        preg_match_all("/<a[\s]+[^>]*?href[\s]?=[\s\"\']+"."(.*?)[\"\']+.*?>"."([^<]+|.*?)?<\/a>/", $response, $matches, PREG_SET_ORDER );

        if($GLOBALS['debug']){
            echo "<br>Term is: " . $term . "<br>";
            echo "ID is: " . $id . "<br>";
            echo "Compact term is: " . $compact . "<br>";
            echo "First term is: " . $string_start . "<br><hr>";
        }

        foreach($matches as $match){

            $href = $match[1];
            $output[] = $href;
        }


               foreach($output as $link){

                    // First Check, get rid of AJAX links and Bookmarks
                    if($link !== "#" && $link !== '/bookmarks/pages' ){

                        if( (stripos(strtolower($link), $url_term) !== false && stripos(strtolower($link), 'pages') !== false  ) || (stripos($link,  '.com/'.$url_term) !== false ) || ( stripos(strtolower($link), $compact) !== false ) || stripos(strtolower($link), $string_start) ) {

                            if(stripos(strtolower($link), 'search') == false ){
                                echo "<br>true for: " . $link . "<br>";
                                $final[] = $link;
                            }
                        }

                    }
               }

            } else {
                $output = "No Response";
                return false;
            }


    if($id){

        echo "<br> Final count was " . count($final) . "<hr>";



        if(first_result($final, 0) !== false){
            $url = first_result($final, 1);
        } else {
            echo "<hr><h2 style='color:tomato;'> There were no results to parse, resetting. </h2><hr>";
            return false;
        }

        $search_term = str_replace(' ', '-', $term);

        $final = string_between(strtolower($url), strtolower($search_term)."/", '?');

        echo "<br><h1 style='color:pink;'>{$string_start}</h1><br>";

        if(empty($final)){
            $final = string_between(strtolower($url),'pages/', '?');
            if(!empty($final)){
                $final = 'pages/'. $final;
                echo "<br><span style='color:orange;'> Second var failed, amending to: {$final}</span><br>";
            }
        }

        if(empty($final)) {


            echo "<br>{$term}";

            $url_parts = explode( $term."/", strtolower($url));

            $final = $url_parts[1];
            echo "<br><span style='color:green;font-weight:bold;'> Second method tried </span><br>";
        }

        if(empty($final)){
            echo "<br> <span style='color:red;font-weight:bold;'> Final is still empty, attempting to correct. </span><br>";
            $final = string_between($url, ".com/", '/');
        }

        if($GLOBALS['debug']){
            echo 'Page id is: ' . $final . "<br> The term is: " . $term . "<br> Queried term is:". $search_term. "<br> Url is: <a href='{$url}' target='_blank'> ".$url."</a><br><hr>";

        }


        return $final;

    } else {
        return $final;
    }

}

function logout($html) {


    preg_match_all("/<a[\s]+[^>]*?href[\s]?=[\s\"\']+"."(.*?)[\"\']+.*?>"."([^<]+|.*?)?<\/a>/", $html, $matches, PREG_SET_ORDER );

    foreach ($matches as $match) {
        $href = $match[0];

            if (0 !== strpos($href, 'http')) {

                $path = '/' . ltrim($href, '/');
                if (extension_loaded('http')) {
                    $href = http_build_url($href , array('path' => $path));
                }

                $output[] = $href;
                }
        }

        foreach($output as $link) {
            if(stripos(strtolower($link), 'logout' )  ){
                $url = $link;
            }
        }

        if(empty($url)) {
            die('Failed to logout');
        }

        $final = string_between($url, '"/', '"');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_USERAGENT, $GLOBALS['uagent']);
        curl_setopt($ch, CURLOPT_URL, 'http://mbasic.facebook.com/'. $final);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_REFERER, "http://www.facebook.com");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 600);
        $result = curl_exec($ch);

        curl_close($ch);

        return $result;

}

function like($id, $term = null) {

    if(empty($id)) {
        return false;
    }


    $seps   = " _+-:*^";


    $term               = urlencode(strtolower($term));
    $sanitized_term     = strtolower(clean($term));
    $sanitized_id       = strtolower(clean($id));
    $string_start       = strtok(strtolower($term), $seps);

    if($GLOBALS['debug']) {
        echo "<br> <b> Started Like Function </b> <hr>";
        echo "<hr> Url Check Term is: " . $term . "<br>";
        echo "Sanitized term is: " . $sanitized_term . "<br>";
        echo "Sanitized ID is: " . $sanitized_id . "<br>";
        echo "The first word is: " . $string_start . "<br>";
        echo "The passed ID is: " . $id . "<hr>";
    }

    if(stripos($id, 'pages/') !== false || stripos($id, $term) !== false || stripos($sanitized_id, $sanitized_term) !== false || stripos($id, $string_start) !== false ){
        $url = 'https://mbasic.facebook.com/'.$id;
    } else{
        $url = 'https://mbasic.facebook.com/profile.php?id='.$id.'&refid=46';
    }

    if($GLOBALS['debug']) {
        echo "<hr> Querying: ". $id . " for: " . $term . "<br>";
        echo $id . "<br>";
        echo "Basic URL is:  <a href='{$url}' target='_blank'>". $url . "<a><hr>";
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 300000);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $GLOBALS['cookies']);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $GLOBALS['cookies']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT,$GLOBALS['uagent']);
    curl_setopt($ch, CURLOPT_REFERER, "http://www.facebook.com");
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6000);

    $result = curl_exec($ch);
    curl_close($ch);

    if($result){
        preg_match_all("/<a[\s]+[^>]*?href[\s]?=[\s\"\']+"."(.*?)[\"\']+.*?>"."([^<]+|.*?)?<\/a>/", $result, $matches, PREG_SET_ORDER );

        foreach ($matches as $match) {
            $href = $match[0];

                if (0 !== strpos($href, 'http')) {

                    $path = '/' . ltrim($href, '/');
                    if (extension_loaded('http')) {
                        $href = http_build_url($href , array('path' => $path));
                    }

                    $output[] = $href;
                    }
            }

                $i = 1;

                echo ($GLOBALS['debug']) ? print_r($output) : "<br> No Links <br>";

                foreach($output as $link){

                    if(stripos(strtolower($link), 'OnLiking' )  && stripos(strtolower($link), 'fan')){
                        $final = $link;
                    } elseif(stripos(strtolower($link), 'unfan')) {
                        echo "<hr style='border:2px solid green;'><b style='font-size:1.7em;'> Topic Already liked, continuing... </b><br><hr style='border:2px solid green;'>";
                        return true;
                    }
                    $i++;

               }

    } else {
        die('No result');
    }

    if($GLOBALS['debug']){
        echo "There where " . $i++ . " links checked. <br>";
        echo 'Converting: '. $final . ' to xml. <br> <hr>';
    }

    if(!isset($final)){
        die('FATAL ERROR: $final is undefined');
    }

    // Strip the HREFs

    $like = new SimpleXMLElement($final);
    $url = 'https://mbasic.facebook.com/'.$like['href'];

    if($GLOBALS['debug']){
        echo "<hr><br><span style='background:green; color:#fff'> Got link: " . $like['href'] . "<br> Proceeding to like: <a href='{$url}' target='_blank'> ".$url."</a></span><br><hr>";
    }


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $GLOBALS['cookies']);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $GLOBALS['cookies']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $GLOBALS['uagent']);
    curl_setopt($ch, CURLOPT_REFERER, "http://www.facebook.com");
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6000);

    $result = curl_exec($ch);
    curl_close($ch);

    return $result;

}

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return $string;

}

function homepage() {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_USERAGENT, $GLOBALS['uagent']);
        curl_setopt($ch, CURLOPT_URL, 'http://mbasic.facebook.com/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        curl_close($ch);
        return $result;
}

function full_homepage() {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_USERAGENT, $GLOBALS['uagent']);
        curl_setopt($ch, CURLOPT_URL, 'http://facebook.com/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        curl_close($ch);
        return $result;
}


function post($status){
        $html = homepage();

        $form_action = parse_action($html, 2);
        $inputs = parse_inputs($html);
        $post_params = "status=$status&";

        foreach ($inputs as $input) {
            $post_params .= $input->getAttribute('name') . '=' . urlencode($input->getAttribute('value')) . '&';
        }

        preg_match('/name="post_form_id" value="(.*)" \/>/', $html, $form_id);
        $fields = '&status=' . urlencode($status) . '&update=' . urlencode("Update status");

        if ($GLOBALS['debug']) {
            echo "\nStatus update form action: $form_action\n";
            echo "\nStatus update params: $post_params\n";
            echo "<br><br><br>" . $form_action . $fields;
        }



        /*
         * post the update
         */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEJAR, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $GLOBALS['cookies']);
        curl_setopt($ch, CURLOPT_USERAGENT, $GLOBALS['uagent']);
        curl_setopt($ch, CURLOPT_URL, $form_action);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields );

        $updated = curl_exec($ch);

        if ($GLOBALS['debug']) {
            echo $updated;
        }
        curl_close($ch);

        return $status;
}

function first_result(array $results, $depth = 0){

    var_dump($results);

    if($GLOBALS['debug']){
        echo "<br><h1> First result was passed array with " . count($results) . " elements </h1><br>";
        echo "<br><hr>";

    }


    if(count($results) == 0) {
        echo "<hr> <span style='color:red; font-weight:bold;'> No results to parse. </span><br>";
        return false;
    }

    if(count($results) > 0){
        (!empty($results[$depth])) ? $output = $results[$depth] : $output = $results[0];

    } else {
        echo "<br> <span style='color:purple;font-size:1.6em;font-weight:bold'> Fatal error: no Links </span><br>";
        return false;
    }

    if($GLOBALS['debug']){
        echo "<div style='background:darkGreen; color:#fff;padding:1em;'> Returned value is: <b> {$output} </b> <br></div>";
    }

    return $output;

}


function parse_inputs($html) {
    $dom = new DOMDocument;
    @$dom->loadxml($html);
    $inputs = $dom->getElementsByTagName('input');
    return($inputs);
}


function parse_action($html, $depth = 0) {
    $dom = new DOMDocument;
    @$dom->loadxml($html);
    $form_action = $dom->getElementsByTagName('form')->item($depth)->getAttribute('action');

    if (!strpos($form_action, "//")) {
        $form_action = "https://m.facebook.com$form_action";
    }
    return($form_action);
}



function string_between($string, $start, $end){

    $string = strtolower($string);

    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}


$user = new User(false, 9);
$bio = $user->bio();

echo fb_login("beaulah@wearepanopticon.com", '(0rnchowder');

// $user->add_interests();





foreach($user->data() as $interest) {
    echo like(fb_search($interest, true), $interest);
}












die();
like(fb_search($interest, true), $interest);

$data = $user->data();
var_dump($data);

die();


die();



die('Completed loop');