<?php

class Session {


	private $_current;

	public static function destroy($name = null){
		if($name) {
			unset($_SESSION[$name]);
		} else {
			session_destroy();
		}
		return true;
	}


	public static function set($name, $value = null){

		if(isset($value)){
			$_SESSION[$name] = $value;
			return true;
		} else {
			return false;
		}
	}

	public static function get($name){
			return $_SESSION[$name];
	}

	public static function exists($name) {
		return (isset($_SESSION[$name])) ? true : false;
	}


}


